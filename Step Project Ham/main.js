/* Our Services */
const myTabs = document.getElementsByClassName('card-collapse');
for (let i = 0; i < myTabs.length; i++) {
    myTabs[i].addEventListener('click', switchTabs);
}

function switchTabs(e) {
    e.preventDefault();
    const tabs = this.parentElement;
    const tabsChildren = tabs.children;
    for (let i = 0; i < tabsChildren.length; i++) {
        tabsChildren[i].classList.remove('active');
    }
    this.classList.add('active');

    const showTabs = document.getElementById(this.dataset.target);
    const show = showTabs.parentElement;
    const showChildren = show.children;
    for (let i = 0; i < showChildren.length; i++) {
        showChildren[i].classList.remove('active');
    }
    showTabs.classList.add('active');
}

/* Our Amazing Work */
const portfolioMenu = document.querySelectorAll('.nav-tabs .nav-tabs-links');
portfolioMenu.forEach(item => item.addEventListener('click', function(e) {
    e.preventDefault();
    const menu = this.closest('.nav-tabs');
    const menuItem = menu.querySelector('.nav-tabs-links.active');
    menuItem.classList.remove('active');
    this.classList.add('active');
    const selector = this.dataset.filter;
    const portfolio = document.querySelectorAll('.category');
    portfolio.forEach(item => item.style.display = 'none');
    const portfolioItem = document.querySelectorAll(`${selector}.category`);
    portfolioItem.forEach(elem => elem.style.display = 'block');
}));

/* Our Amazing Work btn-load-more */

const btn = document.getElementById('btn-load-more');


btn.addEventListener('click', function(e) {
    e.preventDefault();
    const items = document.querySelectorAll('.category');
    items.forEach(item => item.classList.remove('hidden'));
})

/* carusel */
//*   slick   *//
// $(document).ready(function() {
//     $('.slider-for').slick({
//         slidesToShow: 1,
//         slidesToScroll: 1,
//         arrows: false,
//         fade: true,
//         asNavFor: '.slider-nav'
//     });
//     $('.slider-nav').slick({
//         slidesToShow: 3,
//         slidesToScroll: 1,
//         asNavFor: '.slider-for',
//         dots: true,
//         centerMode: true,
//         focusOnSelect: true
//     });
// });

const slider = document.getElementById("header-slider");
const allSlides = getAllChildrenElements(slider, ".blog-slide");
const slidesLastElementIndex = allSlides.length - 1;
const allDots = getAllChildrenElements(slider, ".blog-slider-dots-item");

const sliderNextArrow = document.querySelector("#header-slider .arrow-right");
sliderNextArrow.addEventListener("click", function() {
    const currentActiveSlide = findActiveElement(allSlides, "active");
    const currentActiveSlideIndex = findElementIndex(allSlides, currentActiveSlide);
    let nextActiveSlideIndex = currentActiveSlideIndex + 1;
    if (nextActiveSlideIndex > slidesLastElementIndex) {
        nextActiveSlideIndex = 0;
    }
    setActiveElement(allSlides, nextActiveSlideIndex, "active");
    setActiveElement(allDots, nextActiveSlideIndex, "active");
});

const sliderPrevArrow = document.querySelector("#header-slider .arrow-left");
sliderPrevArrow.addEventListener("click", function() {
    const currentActiveSlide = findActiveElement(allSlides, "active");
    const currentActiveSlideIndex = findElementIndex(allSlides, currentActiveSlide);
    let nextActiveSlideIndex = currentActiveSlideIndex - 1;
    if (nextActiveSlideIndex < 0) {
        nextActiveSlideIndex = slidesLastElementIndex;
    }
    setActiveElement(allSlides, nextActiveSlideIndex, "active");
    setActiveElement(allDots, nextActiveSlideIndex, "active");
});

allDots.forEach(dot => {
    dot.addEventListener("click", function() {
        const currentElementIndex = findElementIndex(allDots, this);
        setActiveElement(allDots, currentElementIndex, "active");
        setActiveElement(allSlides, currentElementIndex, "active");
    });
});

function getAllChildrenElements(parentElem, childrenSelector) {
    const allElements = parentElem.querySelectorAll(childrenSelector);
    return allElements;
}

function findActiveElement(allElements, activeElementClass) {
    let activeElementIndex = 0;
    for (let i = 0; i < allElements.length; i++) {
        if (allElements[i].classList.contains(activeElementClass)) {
            activeElementIndex = i;
            break;
        }
    }
    return allElements[activeElementIndex];
}

function findElementIndex(elementsList, element) {
    let elementIndex = 0;
    for (let i = 0; i < elementsList.length; i++) {
        if (elementsList[i] === element) {
            elementIndex = i;
            break;
        }
    }
    return elementIndex;
}

function setActiveElement(elementList, index, activeClass) {
    for (let i = 0; i < elementList.length; i++) {
        elementList[i].classList.remove(activeClass);
    }
    elementList[index].classList.add(activeClass);
}