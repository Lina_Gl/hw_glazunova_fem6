import createElement from "../components/CreateElement.js";
import { ModalLogin, ModalAddVisit } from "../Modal/indexModalReimport.js";

export class Link extends createElement {
    render() {
        const link = this.create()
        this.link = link
        link.addEventListener('click', this.changeHandler)
        return link
    }

    changeHandler = () => {
        // console.log(this.link.textContent)
        if (this.link.textContent === 'Log in') {
            const modal = new ModalLogin()
            document.body.append(modal.getData())
                // this.link.textContent = 'Создать карточку'

        } else {
            const modal = new ModalAddVisit()
            document.body.append(modal.renderModal())
                // this.link.textContent = 'Вход'
        }
    }
}