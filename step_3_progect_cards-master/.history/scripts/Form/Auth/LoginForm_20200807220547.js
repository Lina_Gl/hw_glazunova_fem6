import Form from '../Form.js'
// import {Select} from "../../components/Select.js";
import { Input } from "../../components/component.js"
import { InputEmailTag, InputPasswordTag, InputSubmitTag } from "../../State/TagsObjects/Login/VisitForm.js";
import { loginUser } from '../../functions/indexFunctionReimport.js'
import { LinkTag } from "../../State/TagsObjects/Tags.js";

export class LoginForm extends Form {
    renderForm() {
        const form = this.render()
        this.form = form;
        const inputEmail = new Input(InputEmailTag).render()
        const InputPassword = new Input(InputPasswordTag).render()
        const InputSubmit = new Input(InputSubmitTag).render()
        addEventListener('submit', this.submitHandler)
        form.append(inputEmail, InputPassword, InputSubmit)
        return form
    }

    submitHandler = (e) => {
        e.preventDefault()
        const login = this.form.querySelector('#register-email').value
        const password = this.form.querySelector('#register-password').value
        const data = {
            email: `${login}`,
            password: `${password}`
        }
        loginUser(data).then((response) => {
            console.log(response.data)
            const { token } = response.data
            if (token) {
                localStorage.setItem('token', token)
            }
        }).then(() => {
            const link = document.getElementById('nav-link');
            link.textContent = 'Create New Card'
        })
    }
}

// document.body.append(new LoginForm().renderForm())