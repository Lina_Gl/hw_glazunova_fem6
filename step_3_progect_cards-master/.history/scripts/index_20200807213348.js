import { Select } from './components/component.js'
import { req } from './config/axios.js'
import { LoginForm, SearchForm } from './Form/indexFormReimport.js'
import {} from './functions/indexFunctionReimport.js'
import {} from './Modal/indexModalReimport.js'
import { VisitCardiologist, VisitDentist, VisitTherapist } from './Visited/indexVisitReimport.js'
import Button from "./Button/Button.js";
import { SelectTag, ButtonTag, LinkTag } from './State/state.js'
import { Link } from "./Button/Link.js";
import { getCards } from "./functions/indexFunctionReimport.js";
import { Close } from "./components/Close.js"

const nav = document.querySelector('.navbar');
LinkTag.getContent()
const link = new Link(LinkTag).render()
nav.append(link)
const main = document.querySelector('.main');


getCards().then((res) => {
    console.log(res.data)
    res.data.forEach((item) => {
        const { "select-doctor": doctor } = item
        const container = document.getElementById('cards');
        switch (doctor) {
            case 'therapist':
                const cardTherapist = new VisitTherapist(item).render()
                container.append(cardTherapist)
                break;
            case 'cardiologist':
                const cardCardiologist = new VisitCardiologist(item).render()
                container.append(cardCardiologist)
                break;
            case 'dentist':
                const cardDentist = new VisitDentist(item).render()
                container.append(cardDentist)
                break;
        }
    })
})

main.prepend(new SearchForm().renderForm())
    // document.body.append(new Close().renderClose());