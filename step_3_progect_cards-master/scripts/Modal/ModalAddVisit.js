import Modal from "./Modal.js";
import {SelectVisitForm} from "../Form/indexFormReimport.js";

export class ModalAddVisit extends Modal{
    renderModal() {
        const modal = this.render()
        console.log(modal)
        modal.append(new SelectVisitForm().renderForm())
        return modal
    }
}

// document.body.append(new ModalAddVisit().renderModal())
