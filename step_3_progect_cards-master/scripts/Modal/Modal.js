import createElement from "../components/CreateElement.js";

export default class Modal {
    constructor(props) {
        this.props = {...props}
    }

    close = (e) => {
            if (e.target.classList.contains('modal')) {
                this.elem.remove();
            }
        }

        
        // getContent() {
        //     const content = `<div class="modal-content">
        //                         <span class="close">&times;</span>
        //                        !!!!!!!!!!Место для Формы!!!!!!!!!
        //                     </div>`;
        //     return content;
        // }

    modal = {
        tag: 'div',
        attr: {
            className: 'modal',
            id: 'my-modal'
        },
        // content: this.getContent()
    }

    render() {
        const modal = new createElement(this.modal).create();
        modal.addEventListener('click', this.close)
        this.elem = modal;
        return modal;
    }
}
// --------- Инициализация -------------

// const modalProps = {
//     className: "modal",
//     id: "my-modal"
// };

// const modalRegister = new Modal(modalProps);
// document.body.prepend(modalRegister.render());
       

