import Modal from "./Modal.js";
import {LoginForm} from "../Form/Auth/LoginForm.js"

export class ModalLogin extends Modal{
    getData() {
        const container = this.render()
        const form = new LoginForm().renderForm()
        container.append(form)
        this.container = container
        form.addEventListener('submit', function (e) {
            e.preventDefault()
            container.remove()
            // const email = document.getElementById('email').value;
            // const password = document.getElementById('password').value;
            // const data = {email, password}
            // console.log(data);
            // return data
        })
        return container
    }
}
    




