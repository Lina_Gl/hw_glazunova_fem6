import {req} from "../config/axios.js";

export async function getCards() {
    return await req({
        url: '/cards'
    })
}
