import {req} from "../config/axios.js";

export async function deleteCard(cardId) {
    const response = await req({
        url: `/cards/${cardId}`,
        method: 'DELETE'
    })
    return response
}
// deleteCard(9513)
// deleteCard(9524)
