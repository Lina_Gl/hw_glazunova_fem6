import Button from "../Button.js";
import {deleteCard as delCard} from "../../functions/indexFunctionReimport.js"

export class Delete extends Button{
    createDeleteButton() {
        const deleteBtn = this.render()
        this.elem = deleteBtn
        deleteBtn.addEventListener('click', this.deleteCard)
        return deleteBtn
    }
    deleteCard = () => {
        const id = this.elem.closest('.card').id;
        delCard(id).then((res) => {
            console.log(res)
            const elem = document.getElementById(`${id}`)
            elem.remove()
        })
    }
}
