import createElement from "../components/CreateElement.js";
import {Form as FormObj} from '../State/state.js'
import {createCard} from "../functions/createCard.js";
import {VisitTherapist} from "../Visited/VisitTherapist.js";
import {VisitCardiologist} from "../Visited/VisitCardiologist.js";
import {VisitDentist} from "../Visited/VisitDentist.js";

export default class Form {
    constructor(props) {
        this.props = props
    }

    render() {
        const form = new createElement(FormObj).create()
        form.addEventListener('submit', this.submitHandler)
        return form
    }

    submitHandler = (e) => {
        e.preventDefault()
        const inputs = document.querySelectorAll('input[name], select, textarea')
        const data = {}
        inputs.forEach((item) => {
            console.log(item)
            data[item.name] = item.value
        })
        createCard(data).then((res) => {
            console.log(res.data)
            console.log(data)
            const {"select-doctor": doctor} = data
            const container = document.getElementById('cards');
            switch (doctor) {
                case 'therapist':
                    const cardTherapist = new VisitTherapist(data).render()
                    container.append(cardTherapist)
                    break;
                case 'cardiologist':
                    const cardCardiologist = new VisitCardiologist(data).render()
                    container.append(cardCardiologist)
                    break;
                case 'dentist':
                    const cardDentist = new VisitDentist(data).render()
                    container.append(cardDentist)
                    break;
            }
            const modal = document.getElementById('my-modal')
            modal.remove();
        })
    }
}

