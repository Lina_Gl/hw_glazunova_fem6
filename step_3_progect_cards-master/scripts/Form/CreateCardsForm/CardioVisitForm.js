import {
    Target,
    SelectPriority,
    Pressure,
    IndexWeight,
    DiseasesOfCardiovascularSystem,
    FullName,
    Age,
    SelectTagPriority,
    TextArea as TextAreaInfo,
    ButtonCreate
} from '../../State/state.js'
import {SelectVisitForm} from "./SelectVisitForm.js";
import {Input, Select, TextArea, createElement} from "../../components/component.js";
import {editCard} from "../../functions/editCard.js";
import {VisitCardiologist} from "../../Visited/indexVisitReimport.js";


export class CardioVisitForm extends SelectVisitForm {
    renderCardio(obj) {
        const div = new createElement({tag: 'div'}).create()
        this.div = div
        const purposeInput = new Input(Target).render()
        const selectPriority = new Select(SelectTagPriority, SelectPriority).render()
        const pressureInput = new Input(Pressure).render()
        const indexWeightInput = new Input(IndexWeight).render()
        const DiseasesOfCardiovascularSystemInput = new Input(DiseasesOfCardiovascularSystem).render()
        const fioInput = new Input(FullName).render()
        const ageInput = new Input(Age).render()
        const textArea = new TextArea(TextAreaInfo).render()
        const btn = new Input(ButtonCreate).render()
        if (obj) {
            const {
                "card-age": age,
                "card-description": description,
                "card-doctor": doctor,
                "card-fio": fio,
                "card-priority": priority,
                "card-purpose": purpose,
                "card-illnesses": diseases,
                "card-pressure": pressure,
                "card-index-weight": weight,
                id
            } = obj
            console.log(obj)
            fioInput.value = fio;
            ageInput.value = age;
            purposeInput.value = purpose;
            selectPriority.value = priority;
            textArea.value = description;
            DiseasesOfCardiovascularSystemInput.value = diseases;
            pressureInput.value = pressure;
            indexWeightInput.value = weight;
            btn.addEventListener('click', this.putRequest.bind(this, id))
        }
        div.append(
            fioInput,
            ageInput,
            purposeInput,
            selectPriority,
            pressureInput,
            indexWeightInput,
            DiseasesOfCardiovascularSystemInput,
            textArea,
            btn)
        // console.log(div)
        return div
    }
    putRequest(id) {
        const body = this.div.querySelectorAll('input[name], select, textarea');
        const bodyObj = {}
        body.forEach(item => {
            bodyObj[item.name] = item.value
        })
        // console.log(bodyObj)
        editCard(id, bodyObj).then((response) => {
            console.log(response.data)
            const div = this.div.closest('.card')
            const prevSibling = div.previousElementSibling
            div.remove()
            const therapist = new VisitCardiologist(bodyObj).render()
            if (prevSibling) {
                prevSibling.after(therapist)
            } else {
                const nextSibling = div.nextElementSibling
                nextSibling.before(therapist)
            }
        })

    }
}

new CardioVisitForm().renderCardio()
