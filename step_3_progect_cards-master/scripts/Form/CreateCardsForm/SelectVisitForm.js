import Form from "../Form.js";
import CreateElement from "../../components/CreateElement.js"
import {Select} from "../../components/Select.js";
import {SelectTag, Option} from "../../State/state.js";
import {CardioVisitForm, DentistVisitForm, TherapistVisitForm} from "../indexFormReimport.js";
import {Close} from "../../components/Close.js";

export class SelectVisitForm extends Form {
    renderForm() {
        const form = this.render()
        const select = new Select(SelectTag, Option).render()
        this.select = select
        form.append(select);
        const close = new Close().renderClose()
        select.addEventListener('change', this.changeHandler.bind(this))
        this.form = form
        this.form.append(close)
        return form
    }

    changeHandler() {
        switch (this.select.value) {
            case 'default':
                if (this.div) {
                    this.div.innerHTML = ''
                }
                break;
            case 'cardiologist':
                if (this.div) {
                    this.div.innerHTML = ''
                }
                this.div = new CardioVisitForm().renderCardio()
                break;
            case 'dentist':
                if (this.div) {
                    this.div.innerHTML = ''
                }
                this.div = new DentistVisitForm().renderDentist()
                break;
            case 'therapist':
                if (this.div) {
                    this.div.innerHTML = ''
                }
                this.div = new TherapistVisitForm().renderTherapist()
                break;
        }
        this.select.after(this.div)

    }
}
