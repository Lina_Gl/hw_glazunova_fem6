import {SearchFormField} from "../../State/Form/Form.js"
import {Input, createElement} from "../../components/component.js"
import {SelectTagPriority, SelectPrioritySearch} from '../../State/state.js'
import {SearchInput} from "../../State/InputFilds/universalFilds.js"
import {Select} from "../../components/Select.js";
// import { Search } from "../../Button/Search/Search.js";

export class SearchForm {
    renderForm(){
        const Searchform = new createElement(SearchFormField).create()
        const searchInput = new Input(SearchInput).render()
        this.elem = searchInput
        const selectPriority = new Select(SelectTagPriority, SelectPrioritySearch).render()
        searchInput.addEventListener('keyup', this.handleKeyUp)
        selectPriority.addEventListener('change',this.handlChange)

        Searchform.append(searchInput,selectPriority)
        console.log(Searchform);
        
        return Searchform
    }

    handleKeyUp = () => {
        const form = this.elem.closest('.search-form');
        const input = form.querySelector('.search-input').value
    
        // console.log(input, preority);
    
        const body = document.querySelectorAll('.card h3')
        const filterarr = [...body]
        const res = filterarr.filter((item) => {
            const div = item.closest('.card')
            div.classList.remove("hide")
            return !item.textContent.includes(input) 
        })
        res.forEach( el => {
            const div = el.closest('.card')
            div.classList.add("hide")
        })
        console.log(res);
        }
    handlChange =() =>{
        const form = this.elem.closest('.search-form');
        const preority = form.querySelector('select').value 
        console.log(preority);

             
        const body= document.querySelectorAll('.card .card-priority')
        console.log(body);
        const filterarr = [...body]
        const res = filterarr.filter((item) => {
            const selectedValue = item.textContent.split(': ').splice(-1,1).join("")
            const div = item.closest('.card')
            div.classList.remove("hide")
            return selectedValue !== preority && preority !== "default"
        })
        res.forEach( el => {
            const div = el.closest('.card')
            div.classList.add("hide")
        })

        
        

    }
}

