import {createElement} from "./component.js"
import Modal from "../Modal/Modal.js"

export class Close extends createElement {
    closeWrapper = {
        tag: 'div',
        attr :{
            className: 'close-wrapper'  
        }
    }

    closeLeft ={
        tag : 'span',
        attr : {
            className: "close-left"
        }
    }

    closeRight ={
        tag : 'span',
        attr : {
            className: "close-right"
        }
    }
   renderClose(){
       const closeWrapper = new createElement(this.closeWrapper).create()
       this.closeWrapper = closeWrapper
       const closeLeft = new createElement(this.closeLeft).create()
       const closeRight = new createElement(this.closeRight).create()
       closeWrapper.append(closeLeft, closeRight)
       closeWrapper.addEventListener('click', this.handleClose)
       
       return closeWrapper
   }
   handleClose = () => {

       const modal = this.closeWrapper.closest('.modal');
       modal.remove()
    //    if(this.closeWrapper.closest('.modal')){
    //        close()
    //    }
   }
}
