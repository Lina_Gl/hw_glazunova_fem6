import CreateElement from './CreateElement.js'

export class Input extends CreateElement {

    handleFocus = () => {
        if (this.error) {
            this.error.remove()
            this.elem.classList.toggle('error-border')
        }
    };

    handleBlur = () => {
        if (!this.elem.value) {
            this.elem.classList.toggle('error-border')
        }
    };


    render() {
        const { errorText, ...attr } = this.props;
        this.errorText = errorText
        const { attr: attribute } = attr
        const element = this.create();
        if (attribute.required) {
            element.addEventListener("focus", this.handleFocus)
            element.addEventListener("blur", this.handleBlur)
        }
        this.elem = element;
        return element;
    }
};


// ----------Инициализация----------
// const loginInputProps = {
//     type: "text", 
//     className: "form-control", 
//     id: "", 
//     name: "visit-aim", 
//     placeholder: "Введите цель визита", 
//     required: true,
//     errorText: "Поле обязательное для заполнения"            
// };