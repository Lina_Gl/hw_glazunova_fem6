import CreateElement  from './CreateElement.js';
export class Select extends CreateElement {
    // constructor(...props) {
    //     super(...props);
    // }
    render() {
        // const {className, text, id} = this.props;
        const select = this.create();
        // select.addEventListener('change',this.handleChange.bind(this))
        this.elem = select;
        if (this.option) {
            for (const [key, value] of Object.entries(this.option)) {
                this.setOptions(value,key)
            }
        }
        return select
    }

    setOptions(text, value){
        const option = new Option(text,value)
        this.elem.add(option)
    }

    handleChange(){
        // console.log(this.elem.value);
    }
}


// -------------Инициализация---------------

// const selectProps = {
//     className: "options",
//     id: "my-options",
//     name: "doctors",
// };

// const select = new Select(selectProps);
// document.body.append(select.render());
// select.setOptions('выберите врача','zero')
// select.setOptions('cardio','one')
// select.setOptions('dantist','two')
// select.setOptions('terphist','three')
// select.handleChange()
