import createElement  from './CreateElement.js'
export  class TextArea extends createElement {
        render() {
            // const {...attr} = this.props;
            const textarea = this.create();
            this.elem = textarea;
            return textarea;
        }
    }
    
    // -------Инициализация----------
    
    // const textareaProps = {
    //     maxlength: 400,
    //     name: "comments",
    //     placeholder: "Введите коментарий",
    //     cols: 40,
    //     rows: 10,
    //     className: "comments-area",
    //     id: "area",
    //     label: "Comments"
    // }
    
    // const textarea = new Textarea(textareaProps);
    // document.body.append(textarea.render())

