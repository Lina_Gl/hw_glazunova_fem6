export default class createElement {
    constructor(props, option = null) {
        this.props = {...props}
        this.option = {...option}
    }

    create() {
        const {tag, attr, content} = this.props
        // let {content} = this.props
        const elem = document.createElement(tag);
        if (attr) {
            for (const [key, value] of Object.entries(attr)) {
                elem[key] = value;
            }
        }

        if (content) {
            elem.innerHTML = content
        }
        return elem
    }
}
