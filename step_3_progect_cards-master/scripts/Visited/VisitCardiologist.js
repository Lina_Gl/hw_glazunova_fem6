import Visit from "./Visit.js";
import CreateElement from "../components/CreateElement.js";
import {stateCardio} from '../State/state.js';
import {CardButton} from '../Button/indexReimportBtn.js';



export class VisitCardiologist extends Visit{

    render() {
        const res = this.changeState(this.props, stateCardio)
        const {
            titleCard,
            ageCard,
            blockCard,
            doctorCard,
            purposeCard,
            pressureCard,
            indexWeightCard,
            pastIllnessesCard,
            illnessesCard,
            priorityCard,
            descriptionCard
        } = res

        const card = this.createCardContainer(this.props.id)
        const cardTitle = new CreateElement(titleCard).create()
        const cardAge = new CreateElement(ageCard).create()
        const cardBlock = new CreateElement(blockCard).create()
        const cardDoctor = new CreateElement(doctorCard).create()
        const cardPurpose = new CreateElement(purposeCard).create()
        const cardPressure = new CreateElement(pressureCard).create()
        const cardPriority = new CreateElement(priorityCard).create()
        const cardIndexWeight = new CreateElement(indexWeightCard).create()
        // const cardPastIllnesses = new CreateElement(pastIllnessesCard).create()
        const cardIllnesses = new CreateElement(illnessesCard).create()
        const cardDescription = new CreateElement(descriptionCard).create()

        // const cardBlockBtn = new CreateElement(blockCard).create()
        // const {edit, deleteButton} = stateButton;
        // const editBtn = new Edit(edit).createEditButton();
        // const deleteBtn = new Delete(deleteButton).createDeleteButton();
        // cardBlockBtn.append(editBtn, deleteBtn);
        const cardBtn = new CardButton().render()

        cardBlock.append(cardPurpose, cardPriority, cardPressure, cardIndexWeight, cardIllnesses)
        card.append(cardTitle, cardAge, cardDoctor, cardBlock, cardDescription, cardBtn)
        console.log(card)
        return card
    }

    changeState(props, state) {
        const {
            titleCard,
            ageCard,
            doctorCard,
            blockCard,
            purposeCard,
            pressureCard,
            indexWeightCard,
            pastIllnessesCard,
            illnessesCard,
            priorityCard,
            descriptionCard
        } = state;
        const {
            age,
            "full-name": name,
            id,
            "select-priority": priority,
            target,
            pressure,
            "index-weight": weight,
            textarea,
            "diseases-of-cardiovascular-system": illness
        } = props
        titleCard.content = titleCard.getContent(name)
        ageCard.content = ageCard.getContent(age)
        purposeCard.content = purposeCard.getContent(target)
        // blockCard.attr.getId(id)
        priorityCard.content = priorityCard.getContent(priority)
        pressureCard.content = pressureCard.getContent(pressure)
        indexWeightCard.content = indexWeightCard.getContent(weight)
        // pastIllnessesCard.content = pastIllnessesCard.getContent()
        illnessesCard.content = illnessesCard.getContent(illness)
        descriptionCard.content = descriptionCard.getContent(textarea)
        console.log(descriptionCard.content)
        return state
    }
}


// <div class="card">
//     <h3 class="card-fio">Inna Torokina</h3>
// <span class="card-age">Возраст: </span>
// <span class="card-doctor">Pediator</span>
// <div class="card-block">
//     <span class="card-purpose">Цель визита: </span>
//     <span class="card-pressure">Обычное давление: </span>
//     <span class="card-index-weight">Индекс массы тела: </span>
    // <span class="card-past-illnesses">Перенесенные заболевания сердечно-сосудистой системы: </span>
    // <span class="card-illness"> - Сердечный приступ<span>
// </div>
// <a href="#" class="card-detail">Detail more</a>
// </div>
