import CreateElement from '../components/CreateElement.js'
export default class Visit {
    constructor(props) {
        this.props = {...props}
    }

    render() {

    }

    div = {
        tag: 'div',
        attr: {
            className: 'card',
            id: '',
            getId(id) {
                this.id = id
            }
        }
    }

    createCardContainer(id) {
        this.div.attr.getId(id)
        const div = new CreateElement(this.div)
        return div.create()
    }
}
