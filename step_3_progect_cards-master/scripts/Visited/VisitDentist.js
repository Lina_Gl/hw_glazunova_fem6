import Visit from "./Visit.js";
import CreateElement from "../components/CreateElement.js";
import {stateDentist} from '../State/state.js'
import {CardButton} from "../Button/indexReimportBtn.js";
export class VisitDentist extends Visit {

    render() {
        const res = this.changeState(this.props, stateDentist)

        const {
            titleCard,
            doctorCard,
            blockCard,
            purposeCard,
            lastDayCard,
            priorityCard,
            descriptionCard
        } = res

        const card = this.createCardContainer(this.props.id)
        const cardTitle = new CreateElement(titleCard).create()
        const cardDoctor = new CreateElement(doctorCard).create()
        const cardBlock = new CreateElement(blockCard).create()
        const cardPurpose = new CreateElement(purposeCard).create()
        const cardLastDay = new CreateElement(lastDayCard).create()
        const cardPriority = new CreateElement(priorityCard).create()
        const cardDescription = new CreateElement(descriptionCard).create()

        const cardBtn = new CardButton().render()

        cardBlock.append(cardPriority, cardPurpose, cardLastDay)
        card.append(cardTitle, cardDoctor, cardBlock, cardDescription, cardBtn)
        return card
    }

    changeState(props, state) {
        const {
            titleCard,
            priorityCard,
            blockCard,
            purposeCard,
            lastDayCard,
            descriptionCard
        } = state;
        const {"full-name": name, id, "select-priority": priority, target, textarea, "last-day-visited": lastDay} = props
        titleCard.content = titleCard.getContent(name)
        purposeCard.content = purposeCard.getContent(target)
        // blockCard.attr.getId(id)
        priorityCard.content = priorityCard.getContent(priority)
        lastDayCard.content = lastDayCard.getContent(lastDay)
        descriptionCard.content = descriptionCard.getContent(textarea)
        return state
    }
}

// <div class="card">
//     <h3 class="card-fio">Inna Torokina</h3>
// <span class="card-doctor">Врач: Стоматолог</span>
// <div class="card-block">
//     <span class="card-purpose">Цель визита: </span>
// <span class="card-last-day">Дата последнего визита: 12.02.2020</span>
// </div>
// <a href="#" class="card-detail">Detail more</a>
// </div>
