// const axios = require('axios').defaults;
// import axios from 'axios'
export const req = axios.create({
    baseURL: 'http://cards.danit.com.ua',
    headers: {
        // 'Content-Type': 'application/json',
        'Authorization': `Bearer ${localStorage.getItem('token')}`
    }
})
