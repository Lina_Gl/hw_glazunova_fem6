export const SelectTag = {
    tag: 'select',
    attr: {
        className: '',
        id: '',
        name: 'select-doctor'
    }
}

export const SelectTagPriority = {
    tag: 'select',
    attr: {
        className: '',
        id: '',
        name: 'select-priority'
    }
}

export const TextAreaTag = {
    tag: 'textarea',
    attr: {
        maxlength: 400,
        name: "comments",
        placeholder: "Введите коментарий",
        cols: 40,
        rows: 10,
        className: "comments-area",
        id: "area",
        label: "Comments"
    }
}
export const InputTag = {
    tag: 'input',
    attr: {
        type: "text",
        className: "form-control",
        id: "",
        name: "visit-aim",
        placeholder: "Введите цель визита",
        required: true,
    },
    errorText: "Поле обязательное для заполнения"
}

export const ButtonTag = {
    tag: 'button',
    attr: {
        className: '',
        id: '',
    },
    content: 'Создать'
}

export const LinkTag = {
    tag: 'a',
    attr: {
        className: 'navbar-link',
        id: 'nav-link'
    },
    content: '',
    getContent() {
        console.log(!!localStorage.getItem('token'))
        if (!!localStorage.getItem('token')) {
            this.content = 'Создать карточку'
            return this.content
        }
        this.content = 'Войти'
        return this.content
    }
}

export const SearchBtn ={
    tag : 'a',
    attr : {
        className: 'search-btn'
    },
    content : "Поиск"
}

export const Option = {
    default: 'Выберите врача',
    cardiologist: 'Кардиолог',
    dentist: 'Стоматолог',
    therapist: 'Терапевт'
}
