export const Pressure = {
    tag: 'input',
    attr: {
        className: 'input-field',
        name: 'pressure',
        required: true,
        placeholder: 'Введите ваше давление'
    },
    errorText: 'Поле обязательно для заполнения!'
}

export const IndexWeight = {
    tag: 'input',
    attr: {
        className: 'input-field',
        name: 'index-weight',
        required: true,
        placeholder: 'Введите ваш индекс массы тела',
    },
    errorText: 'Поле обязательно для заполнения!'
}

export const DiseasesOfCardiovascularSystem = {
    tag: 'input',
    attr: {
        className: 'input-field',
        name: 'diseases-of-cardiovascular-system',
        required: true,
        placeholder: 'Введите ваши заболевания',
    },
    errorText: 'Поле обязательно для заполнения!'
}
